﻿namespace memorygamev2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbltitle = new System.Windows.Forms.Label();
            this.lbl4x3 = new System.Windows.Forms.Label();
            this.lbl4x5 = new System.Windows.Forms.Label();
            this.lbl5x8 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.timerflipcard = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.playerscore2 = new System.Windows.Forms.Label();
            this.playerscore1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbltitle
            // 
            this.lbltitle.BackColor = System.Drawing.Color.LightBlue;
            this.lbltitle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbltitle.Font = new System.Drawing.Font("Mistral", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltitle.ForeColor = System.Drawing.Color.Brown;
            this.lbltitle.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lbltitle.Location = new System.Drawing.Point(123, 41);
            this.lbltitle.Name = "lbltitle";
            this.lbltitle.Size = new System.Drawing.Size(671, 136);
            this.lbltitle.TabIndex = 0;
            this.lbltitle.Text = "NBA Memory Game";
            this.lbltitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbltitle.Click += new System.EventHandler(this.lbltitle_Click);
            // 
            // lbl4x3
            // 
            this.lbl4x3.BackColor = System.Drawing.Color.LightGray;
            this.lbl4x3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl4x3.Font = new System.Drawing.Font("Mistral", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl4x3.Location = new System.Drawing.Point(304, 211);
            this.lbl4x3.Name = "lbl4x3";
            this.lbl4x3.Size = new System.Drawing.Size(300, 70);
            this.lbl4x3.TabIndex = 1;
            this.lbl4x3.Text = "4x3";
            this.lbl4x3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl4x3.Click += new System.EventHandler(this.label1_Click);
            // 
            // lbl4x5
            // 
            this.lbl4x5.BackColor = System.Drawing.Color.DarkGray;
            this.lbl4x5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl4x5.Font = new System.Drawing.Font("Mistral", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl4x5.Location = new System.Drawing.Point(304, 322);
            this.lbl4x5.Name = "lbl4x5";
            this.lbl4x5.Size = new System.Drawing.Size(300, 70);
            this.lbl4x5.TabIndex = 2;
            this.lbl4x5.Text = "5x4";
            this.lbl4x5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl4x5.Click += new System.EventHandler(this.lbl4x5_Click);
            // 
            // lbl5x8
            // 
            this.lbl5x8.BackColor = System.Drawing.Color.DimGray;
            this.lbl5x8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl5x8.Font = new System.Drawing.Font("Mistral", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl5x8.ForeColor = System.Drawing.Color.Black;
            this.lbl5x8.Location = new System.Drawing.Point(304, 430);
            this.lbl5x8.Name = "lbl5x8";
            this.lbl5x8.Size = new System.Drawing.Size(300, 70);
            this.lbl5x8.TabIndex = 3;
            this.lbl5x8.Text = "5x8";
            this.lbl5x8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl5x8.Click += new System.EventHandler(this.lbl5x8_Click);
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::memorygamev2.Properties.Resources.NBALOGO;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.lbl4x3);
            this.panel1.Controls.Add(this.lbl5x8);
            this.panel1.Controls.Add(this.lbltitle);
            this.panel1.Controls.Add(this.lbl4x5);
            this.panel1.Location = new System.Drawing.Point(1, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(970, 551);
            this.panel1.TabIndex = 4;
            // 
            // timerflipcard
            // 
            this.timerflipcard.Interval = 1500;
            this.timerflipcard.Tick += new System.EventHandler(this.timerflipcard_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(854, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "player2";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.label2.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(765, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "player1";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.Visible = false;
            // 
            // playerscore2
            // 
            this.playerscore2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.playerscore2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.playerscore2.Cursor = System.Windows.Forms.Cursors.Default;
            this.playerscore2.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerscore2.Location = new System.Drawing.Point(854, 105);
            this.playerscore2.Name = "playerscore2";
            this.playerscore2.Size = new System.Drawing.Size(63, 34);
            this.playerscore2.TabIndex = 7;
            this.playerscore2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.playerscore2.Visible = false;
            // 
            // playerscore1
            // 
            this.playerscore1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.playerscore1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.playerscore1.Cursor = System.Windows.Forms.Cursors.Default;
            this.playerscore1.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerscore1.Location = new System.Drawing.Point(765, 105);
            this.playerscore1.Name = "playerscore1";
            this.playerscore1.Size = new System.Drawing.Size(63, 34);
            this.playerscore1.TabIndex = 8;
            this.playerscore1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.playerscore1.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::memorygamev2.Properties.Resources.NBALOGO;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(971, 554);
            this.Controls.Add(this.playerscore1);
            this.Controls.Add(this.playerscore2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbltitle;
        private System.Windows.Forms.Label lbl4x3;
        private System.Windows.Forms.Label lbl4x5;
        private System.Windows.Forms.Label lbl5x8;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Timer timerflipcard;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label playerscore2;
        private System.Windows.Forms.Label playerscore1;
    }
}

