﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace memorygamev2
{
    public partial class Form1 : Form
    {
        Random rnd = new Random();
        PictureBox []allp;
        PictureBox picx, picy;
        int count = 0, score1 = 0, score2 = 0;
        bool player1 = true, player2 = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
          
        }    
            private void shuffle()
        {
            int num;
            PictureBox temp;
            for (int i = 0; i < allp.Length; i++)
            {
                num = rnd.Next(0, allp.Length);
                temp = allp[i];
                allp[i] = allp[num];
                allp[num] = temp;
            }
        }
        private void showBoard(int row, int col)
        {
            int x = 140, y = 100;
            int place = 0;
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    allp[place].Image = (Image)Properties.Resources.backcard;
                    allp[place].Location = new Point(x, y);
                    x += 85;
                    Controls.Add(allp[place++]);
                }
                y += 85;
                x = 140;
            }
        }
        private void buildPicture(int size)
        {
            int picNum = 1;
            allp = new PictureBox[size];
            for (int i = 0; i < allp.Length - 1; i += 2)
            {
                allp[i] = new PictureBox();
                allp[i].Image = (Image)Properties.Resources.ResourceManager.GetObject("pic" + (picNum));
                allp[i].Size = new Size(80, 80);
                allp[i].SizeMode = PictureBoxSizeMode.StretchImage;
                allp[i].BorderStyle = BorderStyle.FixedSingle;
                allp[i].Tag = picNum + "";
                allp[i].Click += allp_Click;

                allp[i + 1] = new PictureBox();
                allp[i + 1].Tag = picNum + "";
                allp[i + 1].Image = (Image)Properties.Resources.ResourceManager.GetObject("pic" + (picNum++));
                allp[i + 1].Size = new Size(80, 80);
                allp[i + 1].SizeMode = PictureBoxSizeMode.StretchImage;
                allp[i + 1].BorderStyle = BorderStyle.FixedSingle;          
                allp[i + 1].Click += allp_Click;
            }
        }

        private void allp_Click(object sender, EventArgs e)
        {
           
            if (count == 0)
            {
                picx = (PictureBox)sender;
                picx.Image = (Image)Properties.Resources.ResourceManager.GetObject("pic" + (picx.Tag));
                count++;
            }
            else
                picy = (PictureBox)sender;
                picy.Image = (Image)Properties.Resources.ResourceManager.GetObject("pic" + (picy.Tag));
                if (picx.Tag.Equals(picy.Tag))
                { //צריך להוסיף לייבלים של נקודות לשחקנים 
                if (player1 == true)
                { 
                    score1++;
                    playerscore1.Text = score1 + "";
                }
                else
                {
                    score2++;
                    playerscore2.Text = score2 + "";
                }
                        picx.Visible = false;
                        picy.Visible = false;
                      
                }
                else
                {
                   picx.Image = (Image)Properties.Resources.backcard;
                   picy.Image = (Image)Properties.Resources.backcard;
                   player1 = false;
                   player2 = true;
                }


        }

        private void label1_Click(object sender, EventArgs e)
        {
            buildPicture(12);
            shuffle();
            showBoard(4,3);
            panel1.Visible = false;
        }

        private void lbltitle_Click(object sender, EventArgs e)
        {

        }

        private void lbl4x5_Click(object sender, EventArgs e)
        {
            buildPicture(20);
            shuffle();
            showBoard(4,5);
            panel1.Visible = false;
        }

        private void lbl5x8_Click(object sender, EventArgs e)
        {
            buildPicture(40);
            shuffle();
            showBoard(5,8);
            panel1.Visible = false;
        }

        private void timerflipcard_Tick(object sender, EventArgs e)
        {

        }
    }
}

